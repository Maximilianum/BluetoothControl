package org.maniscalco.android.bluetoothcontrol;

class CommunicationProtocol {

    static final String COMMAND_TERMINATOR = ":";
    static final String COMMAND_NEXT_VALUE = ":";

    public static final String UP_DOWN = "d";
    public static final String ON_OFF = "s";
    public static final String CUSTOM_1 = "c";
}
