package org.maniscalco.android.bluetoothcontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BluetoothFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BluetoothFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BluetoothFragment extends Fragment {

    private static final int DEFAULT_TRANSMISSION_INTERVAL = 250;
    private static final int REQUEST_ENABLE_BT = 1;

    private TextView mBluetoothStatus;
    private EditText mTransmitInterval;
    private ArrayAdapter<String> BDArrayAdapter;
    private BluetoothAdapter mBluetoothAdapter;

    public BluetoothFragment() {
        // Required empty public constructor
    }

    public static BluetoothFragment newInstance() {
        return new BluetoothFragment();
    }

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bluetooth, container, false);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        mBluetoothStatus = view.findViewById(R.id.bluetoothStatus);

        Button mTurnOn = view.findViewById(R.id.turnOn);
        mTurnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                turnOnBluetooth();
            }
        });

        Button mRefresh = view.findViewById(R.id.refresh);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listBluetoothDevices();
            }
        });

        Button mScan = view.findViewById(R.id.scan);
        mScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanNewDevices();
            }
        });

        mTransmitInterval = view.findViewById(R.id.transmitInterval);
        mTransmitInterval.setText(String.valueOf(DEFAULT_TRANSMISSION_INTERVAL));

        ListView mBluetoothDevices = view.findViewById(R.id.bluetoothDevices);
        BDArrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        mBluetoothDevices.setAdapter(BDArrayAdapter);
        mBluetoothDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String devname = (String) adapterView.getItemAtPosition(i);
                int interval = Integer.parseInt(mTransmitInterval.getText().toString());
                if (devname != null) {
                    mBluetoothAdapter.cancelDiscovery();
                    Intent intent = ControlActivity.newIntent(getActivity(), devname, interval);
                    startActivity(intent);
                }
            }
        });

        if (mBluetoothAdapter == null) {
            mBluetoothStatus.setText(R.string.statusNotSupported);
            mTurnOn.setEnabled(false);
            mRefresh.setEnabled(false);
            mScan.setEnabled(false);
            mBluetoothDevices.setEnabled(false);
            Toast toast = Toast.makeText(getActivity(), R.string.msgNotSupported, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, -200);
            toast.show();
        } else {
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothStatus.setText(R.string.enabled);
                listBluetoothDevices();
            } else {
                mBluetoothStatus.setText(R.string.disabled);
            }
        }

        getActivity().registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
        getActivity().registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_ENABLE_BT){
            if(mBluetoothAdapter.isEnabled()) {
                mBluetoothStatus.setText(R.string.enabled);
            } else {
                mBluetoothStatus.setText(R.string.disabled);
            }
        }
    }

    private void listBluetoothDevices() {
        if (mBluetoothAdapter.isEnabled()) {
            BDArrayAdapter.clear();
            Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : mPairedDevices)
                BDArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            Toast toast = Toast.makeText(getActivity(), R.string.pairedDevices, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getActivity(), R.string.BTDisabled, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }
    }

    private void turnOnBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent turnOnIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);

            Toast toast = Toast.makeText(getActivity(),R.string.BTturningOn, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0,0);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getActivity(),R.string.BTAlreadyOn, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0,0);
            toast.show();
        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    Log.d("scanNewDevices", device.getName() + ", " + device.getAddress());
                }
                    // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d("scanNewDevices", "discovery finished");
            }
        }
    };

    private void scanNewDevices() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }

        mBluetoothAdapter.startDiscovery();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }
}
