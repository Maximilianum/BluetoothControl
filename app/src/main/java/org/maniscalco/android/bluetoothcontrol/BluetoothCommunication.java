package org.maniscalco.android.bluetoothcontrol;

import java.util.ArrayList;

class BluetoothCommunication {

    byte[] getBluetoothStreamPacket(int direction, int speed) {

        ArrayList<Byte> output = new ArrayList<>();
        for(byte elem: String.valueOf(direction).getBytes()) {
            output.add(elem);
        }
        output.add(CommunicationProtocol.COMMAND_NEXT_VALUE.getBytes()[0]);
        for(byte elem: String.valueOf(speed).getBytes()) {
            output.add(elem);
        }
        output.add(CommunicationProtocol.COMMAND_TERMINATOR.getBytes()[0]);
        byte[] outputArray =  new byte[output.size()];
        for (int i=0; i < output.size();i++) {
            outputArray[i] = output.get(i);
        }

        return outputArray;
    }
}
