package org.maniscalco.android.bluetoothcontrol;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class ControlActivity extends SingleFragmentActivity implements ControlFragment.OnFragmentInteractionListener {

    private static final String EXTRA_BLUETOOTH_DEVICE = "org.maniscalco.android.bluetoothcontrol.bluetooth_device";
    private static final String EXTRA_TRANSMISSION_INTERVAL = "org.maniscalco.android.bluetoothcontrol.transmission_interval";

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        String deviceName = (String)getIntent().getSerializableExtra(EXTRA_BLUETOOTH_DEVICE);
        int transmissionInterval = (int)getIntent().getSerializableExtra(EXTRA_TRANSMISSION_INTERVAL);
        return ControlFragment.newInstance(deviceName, transmissionInterval);
    }

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }*/

    public static Intent newIntent(Context packageContext, String deviceName, int transmissionInterval) {
        Intent intent = new Intent(packageContext, ControlActivity.class);
        intent.putExtra(EXTRA_BLUETOOTH_DEVICE, deviceName);
        intent.putExtra(EXTRA_TRANSMISSION_INTERVAL, transmissionInterval);
        return intent;
    }

}
