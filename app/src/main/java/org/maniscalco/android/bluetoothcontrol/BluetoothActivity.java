package org.maniscalco.android.bluetoothcontrol;

import android.net.Uri;

public class BluetoothActivity extends SingleFragmentActivity implements BluetoothFragment.OnFragmentInteractionListener {

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return BluetoothFragment.newInstance();
    }

}
