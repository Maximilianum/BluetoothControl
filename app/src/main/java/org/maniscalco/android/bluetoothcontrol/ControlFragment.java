package org.maniscalco.android.bluetoothcontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ControlFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ControlFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControlFragment extends Fragment implements OnTouchPadChangeListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String LOG_EXCEPTION = "ERROR";
    private static final String DEVICE_NAME = "org.maniscalco.android.bluetoothcontrol.devicename";
    private static final String TRANSMISSION_INTERVAL = "org.maniscalco.android.bluetoothcontrol.transmissioninterval";
    private static final String SAVED_STEERING_VALUE = "org.maniscalco.android.bluetoothcontrol.savedsteeringvalue";
    private static final String SAVED_MOTOR_VALUE = "org.maniscalco.android.bluetoothcontrol.savedmotorvalue";
    private static final int minSteeringValue = 20;

    //private String mDeviceName;
    private int mTransmissionInterval;
    private String stdSerialPortId;
    private BluetoothSocket bluetoothSocket;
    private OutputStream outputStream;
    private InputStream inputStream;
    //private Thread bluetoothListener;
    private boolean stopListener;
    private byte[] listenerBuffer;
    private int listenerBufferPosition;
    private boolean shouldUpdate;
    private BluetoothCommunication bluetoothCommunication;
    private int motor, steering;
    private TextView voltageValueView;
    //private TouchPad controlTouchPad;
    private TextView motorValueView;
    private TextView steeringValueView;

    public ControlFragment() {
        // Required empty public constructor
    }

    public static ControlFragment newInstance(String btdevname, int trnsintrv) {
        ControlFragment fragment = new ControlFragment();
        Bundle args = new Bundle();
        args.putString(DEVICE_NAME, btdevname);
        args.putInt(TRANSMISSION_INTERVAL, trnsintrv);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            String deviceName = getArguments().getString(DEVICE_NAME);
            mTransmissionInterval = getArguments().getInt(TRANSMISSION_INTERVAL);
            String deviceData[] = deviceName.split("\\r?\\n");
            bluetoothCommunication = new BluetoothCommunication();
            shouldUpdate = true;
            stdSerialPortId = MetaDataContainer.get(getActivity().getApplicationContext(), "org.maniscalco.android.bluetoothcontrol.standard_serial_port_service_id");
            motor = 50;
            steering = 30;
            connectToBluetoothDevice(deviceData[1]);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_control, container, false);

        voltageValueView = view.findViewById(R.id.voltageValue);

        if (savedInstanceState != null) {
            steering = savedInstanceState.getInt(SAVED_STEERING_VALUE);
            motor = savedInstanceState.getInt(SAVED_MOTOR_VALUE);
        }

        TouchPad controlTouchPad = view.findViewById(R.id.controlTouchPad);
        controlTouchPad.setOnTouchPadChangeListener(this);

        motorValueView = view.findViewById(R.id.motorValue);
        motorValueView.setText(String.format("%1$03d", motor));

        steeringValueView = view.findViewById(R.id.steeringValue);
        steeringValueView.setText(String.format("%1$03d", steering));

        return view;
    }

    private void connectToBluetoothDevice(String serial) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(serial);
        UUID uuid = UUID.fromString(stdSerialPortId);
        try {
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            bluetoothSocket.connect();
            outputStream = bluetoothSocket.getOutputStream();
            inputStream = bluetoothSocket.getInputStream();
        } catch (IOException e) {
            Toast toast = Toast.makeText(getActivity(), R.string.commError, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        }
        startBluetoothListening();
        bluetoothTalker.post(bluetoothTalkerCode);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(SAVED_STEERING_VALUE, steering);
        savedInstanceState.putInt(SAVED_MOTOR_VALUE, motor);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            stopListener = true;
            bluetoothSocket.close();

        } catch (IOException e) {
            Log.e(LOG_EXCEPTION, e.getMessage());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
    }

    private void startBluetoothListening() {

        final Handler handler = new Handler();
        stopListener = false;
        listenerBufferPosition = 0;
        listenerBuffer = new byte[1024];
        Thread bluetoothListener = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted() && !stopListener) {
                    try {
                        int bytesAvailable = inputStream.available();
                        if (bytesAvailable > 0) {
                            byte[] packetBytes = new byte[bytesAvailable];
                            final int read = inputStream.read(packetBytes);
                            for (int i=0; i<bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == CommunicationProtocol.COMMAND_TERMINATOR.getBytes()[0]) {
                                    byte[] encodedBytes = new byte[listenerBufferPosition];
                                    System.arraycopy(listenerBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, StandardCharsets.UTF_8);
                                    listenerBufferPosition = 0;
                                    handler.post(new Runnable() {
                                        public void run() {
                                            voltageValueView.setText(data);
                                        }
                                    });
                                } else {
                                    listenerBuffer[listenerBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex) {
                        stopListener = true;
                    }
                }
            }
        });

        bluetoothListener.start();
    }

    private final Handler bluetoothTalker = new Handler();
    private final Runnable bluetoothTalkerCode = new Runnable() {
        @Override
        public void run() {
            try {
                if (shouldUpdate) {
                    outputStream.write(bluetoothCommunication.getBluetoothStreamPacket(steering + minSteeringValue, motor));
                }
            } catch (IOException $e) {
                onCommunicationError();
            }
            shouldUpdate = false;
            bluetoothTalker.postDelayed(bluetoothTalkerCode, mTransmissionInterval);
        }
    };

    private void onCommunicationError(){
        Toast.makeText(getActivity(), R.string.commError, Toast.LENGTH_LONG).show();
        SystemClock.sleep(2000);
        getActivity().onBackPressed();
    }

    @Override
    public void onTouchPadProgressChanged(int progressX, int progressY) {
        //Log.d("ProgressChanged", "progress x: " + progressX + " y: " + progressY);
        steering = progressX;
        motor = progressY;
        steeringValueView.setText(String.format("%1$03d", progressX));
        motorValueView.setText(String.format("%1$03d", progressY));
        shouldUpdate = true;
    }
}
