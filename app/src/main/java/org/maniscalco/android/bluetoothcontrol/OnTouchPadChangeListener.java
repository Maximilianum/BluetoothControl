package org.maniscalco.android.bluetoothcontrol;

public interface OnTouchPadChangeListener {

    void onTouchPadProgressChanged(int progressX, int progressY);
}
