package org.maniscalco.android.bluetoothcontrol;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;


public class TouchPad extends View {

    private OnTouchPadChangeListener changeListener;
    private int mMaxX;
    private int mMaxY;
    private int mProgressX;
    private int mProgressY;
    private Paint mRectPaint;
    private Paint mShadowPaint;
    private Paint mPointerPaint;
    private int mPointerOffColour;
    private int mPointerOnColor;
    private Rect mRectBounds;
    private Rect mShadowBounds;
    private RectF mPointerBounds;
    private float mHorizontalCentre;
    private float mVerticalCentre;
    private GestureDetector mGDetector;

    public TouchPad(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes( attrs, R.styleable.TouchPad, 0, 0);
        try {
            mMaxX = typedArray.getInt(R.styleable.TouchPad_maxX, 100);
            mMaxY = typedArray.getInt(R.styleable.TouchPad_maxY, 100);
            mProgressX = typedArray.getInt(R.styleable.TouchPad_progressX, 50);
            mProgressY = typedArray.getInt(R.styleable.TouchPad_progressY, 50);
        } finally {
            typedArray.recycle();
        }
        init();
    }

    class MyListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent me) {
            return true;
        }
    }

    public void setOnTouchPadChangeListener(OnTouchPadChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    private void init() {

        mRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRectPaint.setColor(0xff00aa00);
        mRectPaint.setStyle(Paint.Style.FILL);

        mShadowPaint = new Paint(0);
        mShadowPaint.setColor(0xaa004400);
        mShadowPaint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));

        mPointerOffColour = 0xffffff00;
        mPointerOnColor = 0xffff8800;

        mPointerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPointerPaint.setColor(mPointerOffColour);
        mPointerPaint.setStyle(Paint.Style.FILL);

        mRectBounds = new Rect();
        mShadowBounds = new Rect();
        mPointerBounds = new RectF();

        mGDetector = new GestureDetector(TouchPad.this.getContext(), new MyListener());
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {

        // Account for padding
        float xpad = (float)(getPaddingLeft() + getPaddingRight());
        float ypad = (float)(getPaddingTop() + getPaddingBottom());

        float ww = w - xpad;
        float hh = h - ypad;

        // calculate pointer bounds
        float diam = (w / 100.0f) * 20.0f;
        mHorizontalCentre = (w / 2.0f);
        mVerticalCentre = (h / 2.0f);
        float pleft = mHorizontalCentre - (diam / 2.0f);
        float ptop = mVerticalCentre - (diam / 2.0f);

        mRectBounds.set((int)xpad, (int)ypad, (int)ww, (int)hh);
        mShadowBounds.set(mRectBounds);
        mShadowBounds.offset(6, 6);
        mPointerBounds.set(pleft, ptop, (pleft + diam), (ptop + diam));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Try for a width based on our minimum
        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);

        // Whatever the width ends up being, ask for a height that would let the view
        // get as big as it can
        int minh = MeasureSpec.getSize(w) - getPaddingBottom() + getPaddingTop();
        int h = resolveSizeAndState(MeasureSpec.getSize(w), heightMeasureSpec, 0);

        setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the shadow
        canvas.drawRect(
                mShadowBounds,
                mShadowPaint
        );

        // Draw the label text
        //canvas.drawText(mData.get(mCurrentItem).mLabel, mTextX, mTextY, mTextPaint);

        // Draw the Rect
        canvas.drawRect(
                mRectBounds,
                mRectPaint
        );

        // Draw the pointer
        canvas.drawOval(
                mPointerBounds,
                mPointerPaint
        );
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = mGDetector.onTouchEvent(event);
        if (result) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                if (mRectBounds.contains((int)event.getX(), (int)event.getY())) {
                    movePointerTo(event.getX(), event.getY());
                    mPointerPaint.setColor(mPointerOnColor);
                    invalidate();
                    //Log.d("TouchPad", "action move X: " + event.getX() + " Y: " + event.getY());
                }
                result = true;
            }
        } else {
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (mRectBounds.contains((int)event.getX(), (int)event.getY())) {
                    movePointerTo(event.getX(), event.getY());
                    invalidate();
                    //Log.d("TouchPad", "action move X: " + event.getX() + " Y: " + event.getY());
                }
                result = true;
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                movePointerTo(mHorizontalCentre, mVerticalCentre);
                mPointerPaint.setColor(mPointerOffColour);
                invalidate();
                result = true;
            }
        }
        return result;
    }

    private void movePointerTo(float newX, float newY) {
        float diam = mPointerBounds.right - mPointerBounds.left;
        float left = newX - (diam / 2);
        float top = newY - (diam / 2);
        mPointerBounds.set(left, top, left + diam, top + diam);

        // calculate progress values
        mProgressX = (mMaxX * (int)newX) / getWidth();
        mProgressY = (mMaxY * (int)newY) / getHeight();

        // invert value for Y
        mProgressY = mMaxY - mProgressY;

        //Log.d("Progress", "Progress x: " + mProgressX + " y: " + mProgressY);

        if (changeListener != null) {
            changeListener.onTouchPadProgressChanged(mProgressX, mProgressY);
        }
    }
}
