package org.maniscalco.android.bluetoothcontrol;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

class MetaDataContainer {

    private static final String META_DATA_LOG = "meta-data-log";

    static String get(Context context, String key) {
        try{
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;

            return bundle.getString(key);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(META_DATA_LOG, "Failed to load meta-data, loading from defaults, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.w(META_DATA_LOG, "Failed to load meta-data loading from defaults, NullPointer: " + e.getMessage());
        }

        throw new RuntimeException("No value found for param: ".concat(key));
    }
}
