Bluetooth Control

Copyright © 2018 – 2019 Massimiliano Maniscalco

I wrote this simple app to remotely control a custom toy car via Bluetooth connection from a smart-phone or tablet.
This is just a part of a project where I customized a cheap RF toy car in a Bluetooth remote controlled car.
For this project I used an Arduino nano compact board as the main controller and a DSD TECH HC-05 Bluetooth wireless board.
I’ll release soon the C source code for the Arduino.
I designed a custom widget which I called ‘TouchPad’ in order to drive the car with just one finger.
Initially I thought to use two control widgets, one for the steering and one for the speed, but after testing my car with the help of my little son,
I found that it’s much easier to use just one control to handle both the steering and the speed.
